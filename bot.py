from telegram import *
from telegram.ext import *
from DialogueWithMaxim import Dialogue


class TelegramBot:
    def __init__(self):
        token = '1362619339:AAHhnVqJNTAzbA6QKagO2K_vXG3IQuHIG7I'
        self.updater = Updater(token=token)
        dispatcher = self.updater.dispatcher
        dispatcher.add_handler(CommandHandler('start', self.start))
        dispatcher.add_handler(CommandHandler('get_sentence', self.get_sentence))

    def run(self):
        self.updater.start_polling()

    def send_message(self, update, msg, **kwargs):
        self.updater.bot.send_message(chat_id=update.message.chat_id,
                                      text=msg, **kwargs)

    def start(self, bot, update):
        keyboard = [["/get_sentence"]]
        reply_markup = ReplyKeyboardMarkup(keyboard)

        self.send_message(update, 'Я не в чем не виновааааат.', reply_markup=reply_markup)
        return

    def get_sentence(self, bot, update):
        dialogue = Dialogue()
        keyboard = [["/get_sentence"]]
        reply_markup = ReplyKeyboardMarkup(keyboard)
        self.send_message(update, dialogue.getPhrase(), reply_markup=reply_markup)
        return
